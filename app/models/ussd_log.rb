class UssdLog < ActiveRecord::Base
    
    validates :msg_type, presence: true
    validates :service_key, presence: true
    validates :msg_type, presence: true
    validates :session_id, presence: true
    validates :mobile_num, presence: true
    validates :ussd_body, presence: true
    validates :nw_code, presence: true
    
  
  def self.to_csv
    attributes = %w{msg_type service_key session_id mobile_num ussd_body nw_code}
    CSV.generate(headers: true) do |csv|
      csv << attributes
      
      all.each do |contact|
        csv << contact.attributes.values_at(*attributes)
      end
      
    end
  end
  
  
  #filter by message type
  def self.msg_type_search(msg_type)
   msg_type = "%#{msg_type}%"
   where('msg_type LIKE ?', msg_type)
  end
  
  #filter by network code
  def self.nw_code_search(nw_code)
   nw_code = "%#{nw_code}%"
   where('nw_code LIKE ?', nw_code)
  end
  
  
  def self.search_date(start, ended)
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
  end
  
  
end
