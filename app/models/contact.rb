class Contact < ActiveRecord::Base
     validates :FIRSTNAME, presence: true
     validates :LASTNAME, presence: true
     validates :PHONE_NUM, presence: true
     validates :MOBILE_NUMBER, presence: true
     validates :AGE, presence: true
     validates :GENDER, presence: true
      
    
  def self.to_csv
    attributes = %w{FIRSTNAME LASTNAME PHONE_NUM MOBILE_NUMBER AGE GENDER}
    CSV.generate(headers: true) do |csv|
      csv << attributes
      
      all.each do |contact|
        csv << contact.attributes.values_at(*attributes)
      end
      
    end
  end
  
  
  #filter by firstname
  def self.firstname_search(firstname)
   firstname = "%#{firstname}%"
   where('FIRSTNAME LIKE ?', firstname)
  end
  
  #filter by lastname
  def self.lastname_search(lastname)
   lastname = "%#{lastname}%"
   where('LASTNAME LIKE ?', lastname)
  end
  
  
  def self.search_date(start, ended)
      where(CREATED_AT: "#{start} 00:00:00".."#{ended} 23:59:59")
  end
  
  
  
end
