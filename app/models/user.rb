class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
        belongs_to :role, class_name: "Role", foreign_key: :role_id
        
        def active_for_authentication?
          super && status
        end
        
         def self.to_csv
            attributes = %w{ fullname username email role_name }
            CSV.generate(headers: true) do |csv|
              csv << attributes
              
              joins(:role).select("users.*, roles.role_name").each do |user|
                csv << user.attributes.values_at(*attributes)
              end
              
            end
          end
        
              
        
        #filter by username
        def self.username_search(username)
         username = "%#{username}%"
         where('username LIKE ?', username)
        end
        
        #filter by network code
        def self.role_search(role)
         role = "%#{role}%"
         joins(:role).where('role_name LIKE ?', role)
        end
        
        def self.search_date(start, ended)
         where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
        end
        
        
  
end
