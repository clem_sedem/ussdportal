class Role < ActiveRecord::Base
  
  has_many :users
  
  def self.to_csv
     attributes = %w{ role_name }
     CSV.generate(headers: true) do |csv|
     csv << attributes
              
     all.each do |role|
     csv << role.attributes.values_at(*attributes)
      end
              
     end    
  end
  
  
  #filter by username
  def self.role_name_search(role_name)
    role_name = "%#{role_name}%"
    where('role_name LIKE ?', role_name)
  end
        
 def self.search_date(start, ended)
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
  end
  
end
