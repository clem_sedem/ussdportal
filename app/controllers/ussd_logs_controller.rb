class UssdLogsController < ApplicationController
  before_action :set_ussd_log, only: [:show, :edit, :update, :destroy]

  # GET /ussd_logs
  # GET /ussd_logs.json
  def index
    @ussd_logs = UssdLog.all.paginate(page: params[:page], per_page: 10).order('id desc')
    
    if params[:count]
      params[:count]
   else
     params[:count] = 10
   end
   
   if params[:page]
     page = params[:page].to_i
   else
     page = 1
   end
   
   if params[:per_page].present?
      # perpage = params[:per_page]
      @per_page = params[:per_page] || UssdLog.per_page || 10
      @ussd_logs = UssdLog.paginate( :per_page => @per_page, :page => params[:page])
   else
     perpage = 10
   end
   @per_page = params[:per_page] || UssdLog.per_page || 10
   page = if params[:page]
            params[:page].to_i
           else
            1
           end
#            
   # per_page = 5
#    
   # offset = (page - 1) * per_page
   # limit = page * per_page
   # @array = *(offset...limit)

   if  params[:search_value] && params[:search_value].strip != ''
     
     if params[:search_param] == 'msg-type'
       @ussd_logs = UssdLog.msg_type_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('ID asc')
       
      elsif params[:search_param] == 'nw-code'
        logger.info "We are INSIDE THE NUMBER PARAM"
        @ussd_logs = UssdLog.nw_code_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('ID asc')
    
   else
      @ussd_logs = UssdLog.paginate(page: params[:page], per_page: params[:count]).order('ID desc')
      @search_json = []
     end
     
         
     elsif params[:search_param] == 'date'
       start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
       ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
       @ussd_logs =UssdLog.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id desc')

     
   end
   p "JSON ARRAY: #{@search_json}"
        
    
    respond_to do |format|
      format.html
      format.csv { send_data @ussd_logs.to_csv}
    end
    
  end


#enable ussd_log
 def enable_log
     log_id=params[:id]
    if UssdLog.update(log_id, :status => 1)
      redirect_to ussd_logs_path, notice: 'Ussd Log was successfully enabled.'
    end
 end

#disable ussd_log
 def disable_log
   log_id=params[:id]
  if UssdLog.update(log_id, :status => 0)
    redirect_to ussd_logs_path, notice: 'Ussd Log was successfully disabled.'
  end
 end


  # GET /ussd_logs/1
  # GET /ussd_logs/1.json
  def show
  end

  # GET /ussd_logs/new
  def new
    @ussd_log = UssdLog.new
  end

  # GET /ussd_logs/1/edit
  def edit
  end

  # POST /ussd_logs
  # POST /ussd_logs.json
  def create
    @ussd_log = UssdLog.new(ussd_log_params)

    respond_to do |format|
      if @ussd_log.save
        format.html { redirect_to ussd_logs_path, notice: 'Ussd log was successfully created.' }
        format.json { render :show, status: :created, location: @ussd_log }
      else
        format.html { render :new }
        format.json { render json: @ussd_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ussd_logs/1
  # PATCH/PUT /ussd_logs/1.json
  def update
    respond_to do |format|
      if @ussd_log.update(ussd_log_params)
        format.html { redirect_to ussd_logs_path, notice: 'Ussd log was successfully updated.' }
        format.json { render :show, status: :ok, location: @ussd_log }
      else
        format.html { render :edit }
        format.json { render json: @ussd_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ussd_logs/1
  # DELETE /ussd_logs/1.json
  def destroy
    @ussd_log.destroy
    respond_to do |format|
      format.html { redirect_to ussd_logs_url, notice: 'Ussd log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ussd_log
      @ussd_log = UssdLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ussd_log_params
      params.require(:ussd_log).permit(:msg_type, :service_key, :session_id, :mobile_num, :ussd_body, :nw_code, :status)
    end
end
