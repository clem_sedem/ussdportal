class TmpContactsController < ApplicationController
  before_action :set_tmp_contact, only: [:show, :edit, :update, :destroy]

  # GET /tmp_contacts
  # GET /tmp_contacts.json
  def index
    @tmp_contacts = TmpContact.all
  end

  # GET /tmp_contacts/1
  # GET /tmp_contacts/1.json
  def show
  end

  # GET /tmp_contacts/new
  def new
    @tmp_contact = TmpContact.new
  end

  # GET /tmp_contacts/1/edit
  def edit
  end

  # POST /tmp_contacts
  # POST /tmp_contacts.json
  def create
    @tmp_contact = TmpContact.new(tmp_contact_params)

    respond_to do |format|
      if @tmp_contact.save
        format.html { redirect_to @tmp_contact, notice: 'Tmp contact was successfully created.' }
        format.json { render :show, status: :created, location: @tmp_contact }
      else
        format.html { render :new }
        format.json { render json: @tmp_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tmp_contacts/1
  # PATCH/PUT /tmp_contacts/1.json
  def update
    respond_to do |format|
      if @tmp_contact.update(tmp_contact_params)
        format.html { redirect_to @tmp_contact, notice: 'Tmp contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @tmp_contact }
      else
        format.html { render :edit }
        format.json { render json: @tmp_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tmp_contacts/1
  # DELETE /tmp_contacts/1.json
  def destroy
    @tmp_contact.destroy
    respond_to do |format|
      format.html { redirect_to tmp_contacts_url, notice: 'Tmp contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tmp_contact
      @tmp_contact = TmpContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tmp_contact_params
      params.require(:tmp_contact).permit(:FIRSTNAME, :LASTNAME, :PHONE_NUM, :AGE, :GENDER, :MOBILE_NUMBER, :STATUS, :SESSION_ID)
    end
end
