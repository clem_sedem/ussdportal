class UsersController < ApplicationController
  
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /uusers
  # GET /uusers.json
  def index
    @users = User.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @roles = Role.all
    
    
    if params[:count]
      params[:count]
   else
     params[:count] = 10
   end
   
   if params[:page]
     page = params[:page].to_i
   else
     page = 1
   end
   
   if params[:per_page].present?
      # perpage = params[:per_page]
      @per_page = params[:per_page] || User.per_page || 10
      @users = User.paginate( :per_page => @per_page, :page => params[:page])
   else
     perpage = 10
   end
   @per_page = params[:per_page] || User.per_page || 10
   page = if params[:page]
            params[:page].to_i
           else
            1
           end
#            
   # per_page = 5
#    
   # offset = (page - 1) * per_page
   # limit = page * per_page
   # @array = *(offset...limit)

   if  params[:search_value] && params[:search_value].strip != ''
     
     if params[:search_param] == 'username'
       @users = User.username_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
       
      elsif params[:search_param] == 'role'
        logger.info "We are INSidE THE NUMBER PARAM"
        @users = User.role_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
        
   else
      @users = User.paginate(page: params[:page], per_page: params[:count]).order('id desc')
      @search_json = []
     end
     
      elsif params[:search_param] == 'date'
       start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
       ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
       @users =User.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

     
   end
   p "JSON ARRAY: #{@search_json}"
    
    
    
    
    respond_to do |format|
     format.html
     # format.xlsx
      format.csv { send_data @users.to_csv}
   end
   
  end


#enable user
 def enable_user
     user_id=params[:id]
    if User.update(user_id, :status => 1)
      redirect_to users_path, notice: 'User was successfully enabled.'
    end
 end

#disable user
 def disable_user
   user_id=params[:id]
  if User.update(user_id, :status => 0)
    redirect_to users_path, notice: 'User was successfully disabled.'
  end
 end


  # GET /uusers/1
  # GET /uusers/1.json
  def show
  end

  # GET /uusers/new
  def new
    @user = User.new
    @roles = Role.all
  end

  # GET /uusers/1/edit
  def edit
     @roles = Role.all
  end

  # POST /uusers
  # POST /uusers.json
  def create
    @roles = Role.all
    @user = User.new(user_params)
   

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  
  # reset_password
  def reset_password
    @roles = Role.all
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice: 'Password was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end  

  # PATCH/PUT /uusers/1
  # PATCH/PUT /uusers/1.json
  def update
    @roles = Role.all
    respond_to do |format|
      if @user.update(user_params_edit)
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uusers/1
  # DELETE /uusers/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
       params.require(:user).permit(:username, :email, :password, :password_confirmation, :status, :role_id, :username, :fullname)
    end
    
    def user_params_edit
       params.require(:user).permit(:username, :email, :status, :role_id, :username, :fullname)
    end
  
  
  # def new
  # end
# 
  # def create
  # end
end
