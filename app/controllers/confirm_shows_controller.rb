class ConfirmShowsController < ApplicationController
  before_action :set_confirm_show, only: [:show, :edit, :update, :destroy]

  # GET /confirm_shows
  # GET /confirm_shows.json
  def index
    @confirm_shows = ConfirmShow.all
  end

  # GET /confirm_shows/1
  # GET /confirm_shows/1.json
  def show
  end

  # GET /confirm_shows/new
  def new
    @confirm_show = ConfirmShow.new
  end

  # GET /confirm_shows/1/edit
  def edit
  end

  # POST /confirm_shows
  # POST /confirm_shows.json
  def create
    @confirm_show = ConfirmShow.new(confirm_show_params)

    respond_to do |format|
      if @confirm_show.save
        format.html { redirect_to @confirm_show, notice: 'Confirm show was successfully created.' }
        format.json { render :show, status: :created, location: @confirm_show }
      else
        format.html { render :new }
        format.json { render json: @confirm_show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /confirm_shows/1
  # PATCH/PUT /confirm_shows/1.json
  def update
    respond_to do |format|
      if @confirm_show.update(confirm_show_params)
        format.html { redirect_to @confirm_show, notice: 'Confirm show was successfully updated.' }
        format.json { render :show, status: :ok, location: @confirm_show }
      else
        format.html { render :edit }
        format.json { render json: @confirm_show.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /confirm_shows/1
  # DELETE /confirm_shows/1.json
  def destroy
    @confirm_show.destroy
    respond_to do |format|
      format.html { redirect_to confirm_shows_url, notice: 'Confirm show was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_confirm_show
      @confirm_show = ConfirmShow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def confirm_show_params
      params.require(:confirm_show).permit(:session_id, :mobile_num, :choice)
    end
end
