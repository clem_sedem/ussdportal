class ConfirmEditsController < ApplicationController
  before_action :set_confirm_edit, only: [:show, :edit, :update, :destroy]

  # GET /confirm_edits
  # GET /confirm_edits.json
  def index
    @confirm_edits = ConfirmEdit.all
  end

  # GET /confirm_edits/1
  # GET /confirm_edits/1.json
  def show
  end

  # GET /confirm_edits/new
  def new
    @confirm_edit = ConfirmEdit.new
  end

  # GET /confirm_edits/1/edit
  def edit
  end

  # POST /confirm_edits
  # POST /confirm_edits.json
  def create
    @confirm_edit = ConfirmEdit.new(confirm_edit_params)

    respond_to do |format|
      if @confirm_edit.save
        format.html { redirect_to @confirm_edit, notice: 'Confirm edit was successfully created.' }
        format.json { render :show, status: :created, location: @confirm_edit }
      else
        format.html { render :new }
        format.json { render json: @confirm_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /confirm_edits/1
  # PATCH/PUT /confirm_edits/1.json
  def update
    respond_to do |format|
      if @confirm_edit.update(confirm_edit_params)
        format.html { redirect_to @confirm_edit, notice: 'Confirm edit was successfully updated.' }
        format.json { render :show, status: :ok, location: @confirm_edit }
      else
        format.html { render :edit }
        format.json { render json: @confirm_edit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /confirm_edits/1
  # DELETE /confirm_edits/1.json
  def destroy
    @confirm_edit.destroy
    respond_to do |format|
      format.html { redirect_to confirm_edits_url, notice: 'Confirm edit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_confirm_edit
      @confirm_edit = ConfirmEdit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def confirm_edit_params
      params.require(:confirm_edit).permit(:session_id, :mobile_num, :choice)
    end
end
