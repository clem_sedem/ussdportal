class ConfirmDeletesController < ApplicationController
  before_action :set_confirm_delete, only: [:show, :edit, :update, :destroy]

  # GET /confirm_deletes
  # GET /confirm_deletes.json
  def index
    @confirm_deletes = ConfirmDelete.all
  end

  # GET /confirm_deletes/1
  # GET /confirm_deletes/1.json
  def show
  end

  # GET /confirm_deletes/new
  def new
    @confirm_delete = ConfirmDelete.new
  end

  # GET /confirm_deletes/1/edit
  def edit
  end

  # POST /confirm_deletes
  # POST /confirm_deletes.json
  def create
    @confirm_delete = ConfirmDelete.new(confirm_delete_params)

    respond_to do |format|
      if @confirm_delete.save
        format.html { redirect_to @confirm_delete, notice: 'Confirm delete was successfully created.' }
        format.json { render :show, status: :created, location: @confirm_delete }
      else
        format.html { render :new }
        format.json { render json: @confirm_delete.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /confirm_deletes/1
  # PATCH/PUT /confirm_deletes/1.json
  def update
    respond_to do |format|
      if @confirm_delete.update(confirm_delete_params)
        format.html { redirect_to @confirm_delete, notice: 'Confirm delete was successfully updated.' }
        format.json { render :show, status: :ok, location: @confirm_delete }
      else
        format.html { render :edit }
        format.json { render json: @confirm_delete.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /confirm_deletes/1
  # DELETE /confirm_deletes/1.json
  def destroy
    @confirm_delete.destroy
    respond_to do |format|
      format.html { redirect_to confirm_deletes_url, notice: 'Confirm delete was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_confirm_delete
      @confirm_delete = ConfirmDelete.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def confirm_delete_params
      params.require(:confirm_delete).permit(:session_id, :mobile_num, :choice)
    end
end
