class UusersController < ApplicationController
  before_action :set_uuser, only: [:show, :edit, :update, :destroy]

  # GET /uusers
  # GET /uusers.json
  def index
    @uusers = Uuser.all
  end

  # GET /uusers/1
  # GET /uusers/1.json
  def show
  end

  # GET /uusers/new
  def new
    @uuser = Uuser.new
  end

  # GET /uusers/1/edit
  def edit
  end

  # POST /uusers
  # POST /uusers.json
  def create
    @uuser = Uuser.new(uuser_params)

    respond_to do |format|
      if @uuser.save
        format.html { redirect_to @uuser, notice: 'Uuser was successfully created.' }
        format.json { render :show, status: :created, location: @uuser }
      else
        format.html { render :new }
        format.json { render json: @uuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /uusers/1
  # PATCH/PUT /uusers/1.json
  def update
    respond_to do |format|
      if @uuser.update(uuser_params)
        format.html { redirect_to @uuser, notice: 'Uuser was successfully updated.' }
        format.json { render :show, status: :ok, location: @uuser }
      else
        format.html { render :edit }
        format.json { render json: @uuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uusers/1
  # DELETE /uusers/1.json
  def destroy
    @uuser.destroy
    respond_to do |format|
      format.html { redirect_to uusers_url, notice: 'Uuser was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_uuser
      @uuser = Uuser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def uuser_params
      params.fetch(:uuser, {})
    end
end
