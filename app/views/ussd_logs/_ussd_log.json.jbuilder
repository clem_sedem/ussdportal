json.extract! ussd_log, :id, :msg_type, :service_key, :session_id, :mobile_num, :ussd_body, :nw_code, :status, :created_at, :updated_at
json.url ussd_log_url(ussd_log, format: :json)
