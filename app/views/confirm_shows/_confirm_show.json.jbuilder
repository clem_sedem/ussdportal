json.extract! confirm_show, :id, :session_id, :mobile_num, :choice, :created_at, :updated_at
json.url confirm_show_url(confirm_show, format: :json)
