json.extract! uuser, :id, :created_at, :updated_at
json.url uuser_url(uuser, format: :json)
