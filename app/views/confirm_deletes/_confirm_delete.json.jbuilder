json.extract! confirm_delete, :id, :session_id, :mobile_num, :choice, :created_at, :updated_at
json.url confirm_delete_url(confirm_delete, format: :json)
