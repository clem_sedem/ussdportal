json.extract! tmp_contact, :id, :FIRSTNAME, :LASTNAME, :PHONE_NUM, :AGE, :GENDER, :MOBILE_NUMBER, :STATUS, :SESSION_ID, :created_at, :updated_at
json.url tmp_contact_url(tmp_contact, format: :json)
