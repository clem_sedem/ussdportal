json.extract! tracker, :id, :page, :function, :session_id, :mobile_num, :status, :created_at, :updated_at
json.url tracker_url(tracker, format: :json)
