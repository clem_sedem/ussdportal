json.extract! contact, :id, :FIRSTNAME, :LASTNAME, :PHONE_NUM, :MOBILE_NUMBER, :AGE, :GENDER, :STATUS, :created_at, :updated_at
json.url contact_url(contact, format: :json)
