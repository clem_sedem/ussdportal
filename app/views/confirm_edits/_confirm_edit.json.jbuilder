json.extract! confirm_edit, :id, :session_id, :mobile_num, :choice, :created_at, :updated_at
json.url confirm_edit_url(confirm_edit, format: :json)
