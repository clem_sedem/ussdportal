require 'test_helper'

class ConfirmEditsControllerTest < ActionController::TestCase
  setup do
    @confirm_edit = confirm_edits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:confirm_edits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create confirm_edit" do
    assert_difference('ConfirmEdit.count') do
      post :create, confirm_edit: { choice: @confirm_edit.choice, mobile_num: @confirm_edit.mobile_num, session_id: @confirm_edit.session_id }
    end

    assert_redirected_to confirm_edit_path(assigns(:confirm_edit))
  end

  test "should show confirm_edit" do
    get :show, id: @confirm_edit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @confirm_edit
    assert_response :success
  end

  test "should update confirm_edit" do
    patch :update, id: @confirm_edit, confirm_edit: { choice: @confirm_edit.choice, mobile_num: @confirm_edit.mobile_num, session_id: @confirm_edit.session_id }
    assert_redirected_to confirm_edit_path(assigns(:confirm_edit))
  end

  test "should destroy confirm_edit" do
    assert_difference('ConfirmEdit.count', -1) do
      delete :destroy, id: @confirm_edit
    end

    assert_redirected_to confirm_edits_path
  end
end
