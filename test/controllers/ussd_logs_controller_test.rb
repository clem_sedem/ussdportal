require 'test_helper'

class UssdLogsControllerTest < ActionController::TestCase
  setup do
    @ussd_log = ussd_logs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ussd_logs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ussd_log" do
    assert_difference('UssdLog.count') do
      post :create, ussd_log: { mobile_num: @ussd_log.mobile_num, msg_type: @ussd_log.msg_type, nw_code: @ussd_log.nw_code, service_key: @ussd_log.service_key, session_id: @ussd_log.session_id, status: @ussd_log.status, ussd_body: @ussd_log.ussd_body }
    end

    assert_redirected_to ussd_log_path(assigns(:ussd_log))
  end

  test "should show ussd_log" do
    get :show, id: @ussd_log
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ussd_log
    assert_response :success
  end

  test "should update ussd_log" do
    patch :update, id: @ussd_log, ussd_log: { mobile_num: @ussd_log.mobile_num, msg_type: @ussd_log.msg_type, nw_code: @ussd_log.nw_code, service_key: @ussd_log.service_key, session_id: @ussd_log.session_id, status: @ussd_log.status, ussd_body: @ussd_log.ussd_body }
    assert_redirected_to ussd_log_path(assigns(:ussd_log))
  end

  test "should destroy ussd_log" do
    assert_difference('UssdLog.count', -1) do
      delete :destroy, id: @ussd_log
    end

    assert_redirected_to ussd_logs_path
  end
end
