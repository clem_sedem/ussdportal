require 'test_helper'

class ConfirmDeletesControllerTest < ActionController::TestCase
  setup do
    @confirm_delete = confirm_deletes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:confirm_deletes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create confirm_delete" do
    assert_difference('ConfirmDelete.count') do
      post :create, confirm_delete: { choice: @confirm_delete.choice, mobile_num: @confirm_delete.mobile_num, session_id: @confirm_delete.session_id }
    end

    assert_redirected_to confirm_delete_path(assigns(:confirm_delete))
  end

  test "should show confirm_delete" do
    get :show, id: @confirm_delete
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @confirm_delete
    assert_response :success
  end

  test "should update confirm_delete" do
    patch :update, id: @confirm_delete, confirm_delete: { choice: @confirm_delete.choice, mobile_num: @confirm_delete.mobile_num, session_id: @confirm_delete.session_id }
    assert_redirected_to confirm_delete_path(assigns(:confirm_delete))
  end

  test "should destroy confirm_delete" do
    assert_difference('ConfirmDelete.count', -1) do
      delete :destroy, id: @confirm_delete
    end

    assert_redirected_to confirm_deletes_path
  end
end
