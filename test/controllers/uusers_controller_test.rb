require 'test_helper'

class UusersControllerTest < ActionController::TestCase
  setup do
    @uuser = uusers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:uusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create uuser" do
    assert_difference('Uuser.count') do
      post :create, uuser: {  }
    end

    assert_redirected_to uuser_path(assigns(:uuser))
  end

  test "should show uuser" do
    get :show, id: @uuser
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @uuser
    assert_response :success
  end

  test "should update uuser" do
    patch :update, id: @uuser, uuser: {  }
    assert_redirected_to uuser_path(assigns(:uuser))
  end

  test "should destroy uuser" do
    assert_difference('Uuser.count', -1) do
      delete :destroy, id: @uuser
    end

    assert_redirected_to uusers_path
  end
end
