require 'test_helper'

class TmpContactsControllerTest < ActionController::TestCase
  setup do
    @tmp_contact = tmp_contacts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tmp_contacts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tmp_contact" do
    assert_difference('TmpContact.count') do
      post :create, tmp_contact: { AGE: @tmp_contact.AGE, FIRSTNAME: @tmp_contact.FIRSTNAME, GENDER: @tmp_contact.GENDER, LASTNAME: @tmp_contact.LASTNAME, MOBILE_NUMBER: @tmp_contact.MOBILE_NUMBER, PHONE_NUM: @tmp_contact.PHONE_NUM, SESSION_ID: @tmp_contact.SESSION_ID, STATUS: @tmp_contact.STATUS }
    end

    assert_redirected_to tmp_contact_path(assigns(:tmp_contact))
  end

  test "should show tmp_contact" do
    get :show, id: @tmp_contact
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tmp_contact
    assert_response :success
  end

  test "should update tmp_contact" do
    patch :update, id: @tmp_contact, tmp_contact: { AGE: @tmp_contact.AGE, FIRSTNAME: @tmp_contact.FIRSTNAME, GENDER: @tmp_contact.GENDER, LASTNAME: @tmp_contact.LASTNAME, MOBILE_NUMBER: @tmp_contact.MOBILE_NUMBER, PHONE_NUM: @tmp_contact.PHONE_NUM, SESSION_ID: @tmp_contact.SESSION_ID, STATUS: @tmp_contact.STATUS }
    assert_redirected_to tmp_contact_path(assigns(:tmp_contact))
  end

  test "should destroy tmp_contact" do
    assert_difference('TmpContact.count', -1) do
      delete :destroy, id: @tmp_contact
    end

    assert_redirected_to tmp_contacts_path
  end
end
