require 'test_helper'

class ConfirmShowsControllerTest < ActionController::TestCase
  setup do
    @confirm_show = confirm_shows(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:confirm_shows)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create confirm_show" do
    assert_difference('ConfirmShow.count') do
      post :create, confirm_show: { choice: @confirm_show.choice, mobile_num: @confirm_show.mobile_num, session_id: @confirm_show.session_id }
    end

    assert_redirected_to confirm_show_path(assigns(:confirm_show))
  end

  test "should show confirm_show" do
    get :show, id: @confirm_show
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @confirm_show
    assert_response :success
  end

  test "should update confirm_show" do
    patch :update, id: @confirm_show, confirm_show: { choice: @confirm_show.choice, mobile_num: @confirm_show.mobile_num, session_id: @confirm_show.session_id }
    assert_redirected_to confirm_show_path(assigns(:confirm_show))
  end

  test "should destroy confirm_show" do
    assert_difference('ConfirmShow.count', -1) do
      delete :destroy, id: @confirm_show
    end

    assert_redirected_to confirm_shows_path
  end
end
