class CreateConfirmDeletes < ActiveRecord::Migration
  def change
    create_table :confirm_deletes do |t|
      t.string :session_id
      t.string :mobile_num
      t.string :choice

      t.timestamps null: false
    end
  end
end
