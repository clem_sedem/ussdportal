class CreateConfirmShows < ActiveRecord::Migration
  def change
    create_table :confirm_shows do |t|
      t.string :session_id
      t.string :mobile_num
      t.string :choice

      t.timestamps null: false
    end
  end
end
