class CreateTmpContacts < ActiveRecord::Migration
  def change
    create_table :tmp_contacts do |t|
      t.STRING :FIRSTNAME
      t.STRING :LASTNAME
      t.STRING :PHONE_NUM
      t.INTEGER :AGE
      t.STRING :GENDER
      t.STRING :MOBILE_NUMBER
      t.BOOLEAN :STATUS
      t.STRING :SESSION_ID

      t.timestamps null: false
    end
  end
end
