class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.STRING :FIRSTNAME
      t.STRING :LASTNAME
      t.STRING :PHONE_NUM
      t.STRING :MOBILE_NUMBER
      t.INTEGER :AGE
      t.STRING :GENDER
      t.BOOLEAN :STATUS

      t.timestamps null: false
    end
  end
end
