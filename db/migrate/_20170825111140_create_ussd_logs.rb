class CreateUssdLogs < ActiveRecord::Migration
  def change
    create_table :ussd_logs do |t|
      t.string :msg_type
      t.string :service_key
      t.string :session_id
      t.string :mobile_num
      t.string :ussd_body
      t.string :nw_code
      t.boolean :status

      t.timestamps null: false
    end
  end
end
