class CreateTrackers < ActiveRecord::Migration
  def change
    create_table :trackers do |t|
      t.integer :page
      t.string :function
      t.string :session_id
      t.string :mobile_num
      t.boolean :status

      t.timestamps null: false
    end
  end
end
