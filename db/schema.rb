# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170829133528) do

  create_table "confirm_delete", force: :cascade do |t|
    t.string "session_id", limit: 50
    t.string "mobile_num", limit: 20
    t.string "choice",     limit: 225
  end

  create_table "confirm_edit", force: :cascade do |t|
    t.string "session_id", limit: 20
    t.string "mobile_num", limit: 15
    t.string "choice",     limit: 20
  end

  create_table "confirm_show", force: :cascade do |t|
    t.string "session_id", limit: 20
    t.string "mobile_num", limit: 20
    t.string "choice",     limit: 20
  end

  create_table "contacts", primary_key: "ID", force: :cascade do |t|
    t.string   "FIRSTNAME",     limit: 20
    t.string   "LASTNAME",      limit: 20
    t.string   "PHONE_NUM",     limit: 13
    t.string   "MOBILE_NUMBER", limit: 20
    t.integer  "AGE",           limit: 4
    t.string   "GENDER",        limit: 10
    t.boolean  "STATUS"
    t.datetime "CREATED_AT",               null: false
    t.datetime "UPDATED_AT",               null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "role_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "status"
  end

  create_table "tmp_contacts", primary_key: "ID", force: :cascade do |t|
    t.string   "FIRSTNAME",  limit: 20
    t.string   "LASTNAME",   limit: 20
    t.string   "PHONE_NUM",  limit: 13
    t.integer  "AGE",        limit: 4
    t.string   "GENDER",     limit: 10
    t.string   "MOBILE_NUM", limit: 15
    t.boolean  "STATUS"
    t.string   "SESSION_ID", limit: 50
    t.datetime "CREATED_AT",            null: false
    t.datetime "UPDATED_AT",            null: false
  end

  create_table "trackers", force: :cascade do |t|
    t.integer  "page",       limit: 4
    t.string   "function",   limit: 50
    t.string   "session_id", limit: 50
    t.string   "mobile_num", limit: 225
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "upated_at",              null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "status"
    t.integer  "role_id",                limit: 4
    t.string   "username",               limit: 255
    t.string   "fullname",               limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "ussd_logs", force: :cascade do |t|
    t.string   "msg_type",    limit: 50
    t.string   "service_key", limit: 50
    t.string   "session_id",  limit: 50
    t.string   "mobile_num",  limit: 13
    t.string   "ussd_body",   limit: 50
    t.string   "nw_code",     limit: 30
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "uusers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
