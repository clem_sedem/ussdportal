Rails.application.routes.draw do
  resources :roles
  # get 'users/new'

  # get 'users/create'

  devise_for :users
  get 'home/index'
  root to: 'home#index'

  resources :confirm_shows
  resources :confirm_edits
  resources :confirm_deletes
  resources :ussd_logs
  resources :trackers
  resources :tmp_contacts
  resources :contacts
  # resources :users
  
  devise_scope :user do
    get '/users/sign_up' => 'users#sign_in'
  end
  
  post 'create_user' => 'users#create', as: :create_user
  get '/users' => 'users#index'
  get '/users/new' => 'users#new', :as => 'new_user'
  get '/users/:id/edit' => 'users#edit', :as => 'edit_user'
  # get '/users/:id/edit' => 'users#edit', :as => 'reset_password'

  get '/users/:id' => 'users#show', :as => 'user'
  delete 'users/:id' => 'users#destroy'
  patch '/users/:id' => 'users#update', :as => 'update_user'
  patch '/users/:id' => 'users#reset_password', :as => 'reset_password'
  
  
  
  
  
  get       'enable_contact'    =>  'contacts#enable_contact'
  get       'disable_contact'   =>  'contacts#disable_contact'
  
  get       'enable_log'    =>  'ussd_logs#enable_log'
  get       'disable_log'   =>  'ussd_logs#disable_log'
  
  get       'enable_user'    =>  'users#enable_user'
  get       'disable_user'   =>  'users#disable_user'
  
  get       'enable_role'    =>  'roles#enable_role'
  get       'disable_role'   =>  'roles#disable_role'
  
  
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
